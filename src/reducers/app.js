import {setAuthData} from "../thunks/auth"

import {INITIALIZED_SUCCESS} from '../consts'

const initialState = {
    isInitialized: false
};

export const app = (state = initialState, action) => {

    switch (action.type) {
        case INITIALIZED_SUCCESS:
            return {
                ...state,
                isInitialized: true
            };

        default:
            return state;
    }
};

export const initializedSuccess = () => ({type: INITIALIZED_SUCCESS});


export const initializeApp = () => async (dispatch) => {
    await dispatch(setAuthData());
    dispatch(initializedSuccess())
};


export default app;