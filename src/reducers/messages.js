import{
    ADD_MESSAGE
} from "../consts"


let initialState = {
    dialogs: (() => {
        let arr = [];
        for (let i = 0; i < 6; i++) {
            arr.push({id: i, name: 'Ivan_' + i.toString()});
        }
        return arr;
    })(),
    messages: (() => {
        let arr = [];
        for (let i = 0; i < 6; i++) {
            arr.push({id: i, message: 'Some message_' + i.toString()});
        }
        return arr;
    })()
};

const messages = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE:
            let cntMessageId = state.messages.length;
            let newMessage = {
                id: ++cntMessageId,
                message: action.payload
            };
            return {
                ...state,
                messages: [...state.messages, newMessage]
            };
        default :
            return state;
    }
};

export default messages;