import {SET_USER_DATA, USER_ID_CHANGED, SET_CAPTCHA} from "../consts";


let initialState = {
    userId: null,
    email: null,
    login: null,
    isAuth: false,
    captcha: null
};

const auth = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                ...action.payload
            };
        case SET_CAPTCHA:
            return {
                ...state,
                captcha : action.payload
            };
        default:
            return state;
    }
};

export default auth;
