import {applyMiddleware, combineReducers, createStore} from "redux";
import profile from "./profile";
import messages from "./messages";
import users from "./users";
import auth from "./auth";
import app from "./app";
import thunkMiddleware from "redux-thunk";
import { reducer as form } from 'redux-form'



const reducers = combineReducers({
    profile,
    messages,
    users,
    auth,
    form,
    app
});

const store = createStore(reducers, applyMiddleware(thunkMiddleware));

window.store = store;

export default store;