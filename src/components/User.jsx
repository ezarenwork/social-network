import React from 'react';
import {NavLink} from "react-router-dom";
import userLogo from "../assets/images/userLogo.png";

const User = ({user, followUser, followingInProgress, unfollowUser}) => {
    return (
        <li className="list-group-item" key={user.id}>
            <div className="row">
                <div className="col-sm-auto">
                    <NavLink to={'/profile/' + user.id}>
                        <img src={user.photos.small != null ? user.photos.small : userLogo}
                             className=' align-self-center'
                             alt=''/>
                    </NavLink>
                </div>
                <div className="col">
                    <div className="row">
                        <NavLink to={'/profile/' + user.id}>
                            {user.name}
                        </NavLink>
                    </div>
                    <div className="row">
                        {user.status}
                    </div>
                    <div className="row">
                        {"u.location.country"}
                    </div>
                    <div className="row">
                        {"u.location.city"}
                    </div>
                    <div className="row">
                        {user.followed ?
                            <button className="btn btn-primary btn-sm"
                                    disabled={followingInProgress.some(id => id === user.id)}
                                    onClick={() => {unfollowUser(user.id);}}>Followed</button> :
                            <button className="btn btn btn-outline-primary btn-sm"
                                    disabled={followingInProgress.some(id => id === user.id)}
                                    onClick={() => {followUser(user.id);}}>Follow</button>}
                    </div>
                </div>
            </div>
        </li>
    );
};

export default User;

