import React from 'react';

const ProfilePost = (props) => {
    return (
        <div>
            <img src='https://www.mirprognozov.ru/uploads/images/old/1453187694-1381309907.jpg' alt=""/>
            <span>{props.post}</span>
            <div>
                {props.likesCount}
            </div>
        </div>
    )
};

export default ProfilePost;