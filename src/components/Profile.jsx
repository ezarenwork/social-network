import React from 'react';
import ProfileInfo from "./ProfileInfo";
import PostsContainer from "../containers/PostsContainer";
import Preloader from "./commons/Preloader";


const Profile = (props) => {

    if(!props.profile){
        return <Preloader/>
    }

    return (
        <div>
            <div>
                <ProfileInfo profile={props.profile}
                             // isOwner ={!props.match.params.userId}
                             status={props.status}
                             uploadAvatarImage ={props.uploadAvatarImage}
                             updateStatus={props.updateStatus}
                             updateProfile={props.updateProfile}/>
            </div>
            <div>
                <PostsContainer store={props.store}/>
            </div>
        </div>
    )
};

export default Profile;