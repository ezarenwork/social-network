import React from 'react';
import ProfilePost from "./ProfilePost";
import {Field, reduxForm} from "redux-form";
import {maxLength30, required} from "../helpers/validators";
import {Textarea} from "./commons/FormControls";

const ProfilePosts = ({addPost, posts}) => {
    let postsElements =
        posts.map(p => <ProfilePost post={p.post} key={p.id} likesCount={p.likeCounts}/>);

    const addNewPost = (e) => {
        addPost(e.newPostText);
    };
    return (
        <div>
            <h3>
                My posts
            </h3>
            <AddPostForm onSubmit={addNewPost}/>
            <div>
                {postsElements}
            </div>
        </div>
    )
};

const PostForm = (props) => {
    return <form onSubmit={props.handleSubmit}>
        <Field component={Textarea}
               name='newPostText'
               placeholder='Your text'
               validate={[required, maxLength30]}
        />
        <button>Add post</button>
    </form>
};

export const AddPostForm = reduxForm({form: 'profilePostAddForm'})(PostForm);

export default ProfilePosts;