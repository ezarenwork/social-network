import React from 'react';
import mainLogo from "../assets/images/main_logo.png"
import {NavLink} from "react-router-dom";
import s from '../styles/header.module.css'
import userAvatar from './../assets/images/userLogo.png'
import style from "../styles/sidebar.module.css";

const Header = ({logout, isAuth, login}) => {

    return (
        <nav className={"navbar navbar-expand-lg navbar-dark bg-dark " + s.header}>
            <a className="navbar-brand" href="#">
                <img src={mainLogo} width="30" height="30" alt="Z" className="d-inline-block align-top m-0 p-0"/>
                <span className="text-monospace font-weight-bold">aren</span>
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                    aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <a className="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo02">
                <ul className="nav navbar-nav ml-auto d-flex">
                    <li className="nav-item align-self-center">
                        <NavLink to="/users" className={"nav-link " + s.navR}
                                 activeClassName={style.active}>Users</NavLink>
                    </li>
                    <li className="nav-item">
                        {isAuth
                            ? <div className="dropdown nav-link d-flex">
                                <div id="dropdownMenu1"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img className={s.userAvatar} src={userAvatar} alt="logout"/>
                                </div>
                                <span className={"align-self-center " + s.navR}>{login}</span>
                                <div className="dropdown-menu " aria-labelledby="dropdownMenu1">
                                    <div className={"dropdown-item " + s.logout} onClick={logout}>Log Out</div>
                                </div>

                            </div>

                            : <NavLink to={'/login'} className={"nav-link " + s.navR}>
                                Login
                            </NavLink>
                        }
                    </li>
                </ul>
            </a>
        </nav>

    );
};
export default Header;


{/*    <HeaderFlex>*/
}
{/*        <HeaderSection>*/
}
{/*            <HeaderItem>*/
}
{/*                <Hamburger*/
}
{/*                    active={props.isHamburgerOpen}*/
}
{/*                    type="squeeze"*/
}
{/*                    onClick={() => props.setIsHamburgerOpen }*/
}
{/*                />*/
}
{/*            </HeaderItem>*/
}
{/*            <HeaderItem>*/
}
{/*                <HeaderLogoBrand/>*/
}
{/*            </HeaderItem>*/
}
{/*        </HeaderSection>*/
}
{/*        <HeaderSection>*/
}
{/*            <HeaderItem>*/
}
{/*                <NavLink activeClassName="active" to='/users'>Users</NavLink>*/
}
{/*            </HeaderItem>*/
}
{/*            <HeaderItem>*/
}
{/*                <LoginArea isAuth={props.isAuth} login={props.login}/>*/
}
{/*            </HeaderItem>*/
}
{/*        </HeaderSection>*/
}
{/*    </HeaderFlex>*/
}
{/*);*/
}
{/*};*/
}