import React, {useState} from 'react';
import userLogo from "../assets/images/userLogo.png";
import s from '../styles/profileInfo.module.css'
import Preloader from "./commons/Preloader";
import ProfileStatus from "./ProfileStatus";

const ProfileInfo = ({profile, status, updateStatus, uploadAvatarImage, updateProfile}) => {

    const [editMode, setEditMode] = useState(false);

    if (!profile) {
        return <Preloader/>
    }


    const addImage = (e) => {
        const photo = e.target.files[0];
        uploadAvatarImage(photo);
    };

    const activateEditMode = () => {
        setEditMode(true);
    };

    const formSubmit = (formData) => {
        updateProfile(formData).then(
            () => {
                setEditMode(false);
            }
        );
    };

    return (
        <div className={"d-flex flex-column justify-content-center " + s.profile}>
            <div className="text-center">
                {/*{isOwner && <button className={s.editButton} onClick={activateEditMode}>Edit </button>}*/}
                <h2>{profile.fullName}</h2>
            </div>
            <div>
                <div className="card w-25">
                    <img className={"card-img-top " + s.avatar} src={profile.photos.large !== null
                        ? profile.photos.large
                        : userLogo} alt={profile.fullName}/>
                </div>
                <ProfileStatus updateStatus={updateStatus}
                               userId={profile.userId}
                               status={status}/>
            </div>
            <div>
                {/*{profile.lookingForAJobDescription}*/}
            </div>
        </div>
    );
};

export default ProfileInfo;