import React from 'react';
import {NavLink} from "react-router-dom";
import style from '../styles/sidebar.module.css'

const Sidebar = () => {
    return (
        <div className={style.block}>
            <nav className={style.nav}>
                <div className={style.item}>
                    <NavLink to="/profile" className='nav-link' activeClassName={style.active}>Profile</NavLink>
                </div>
                <div className={style.item}>
                    <NavLink to="/news" className='nav-link' activeClassName={style.active}>News</NavLink>
                </div>
                <div className={style.item}>
                    <NavLink to="/messages" className='nav-link' activeClassName={style.active}>Messages</NavLink>
                </div>
                <div className={style.item}>
                    <NavLink to="/music" className='nav-link' activeClassName={style.active}>Music</NavLink>
                </div>
                <div className={style.item}>
                    <NavLink to="/games" className='nav-link' activeClassName={style.active}>Games</NavLink>
                </div>
                <div className={style.item}>
                    <NavLink to="/settings" className='nav-link' activeClassName={style.active}>Settings</NavLink>
                </div>
            </nav>
        </div>
    );
};

export default Sidebar;