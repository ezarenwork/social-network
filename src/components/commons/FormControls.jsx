import React from "react";
import styles from "../../styles/formControls.module.css"
import EditProfileDataForm from "../ProfileEditData";



export const Textarea = ({input, meta, ...props}) => {
  return <div className="form-group row col-sm-offset-1">
    <textarea {...input} {...props}  className="form-control col-4" rows={"3"}/>
    {meta.touched && meta.error &&
    <div className="alert alert-danger col-sm-offset-1">{meta.error}</div>}
  </div>

};

export const Input = ({input, type, label, meta}) => {
 const hasError = meta.touched && meta.error;
  return (
    <div className="form-group row">
      <div className={"col-sm-4 col-sm-offset-4" } >
        <input {...input}
               type={type}
               placeholder={label}
               className={"form-control" + " " + ( hasError ? styles.invalid : "")}/>
      </div>
      { hasError &&
      <div className={styles.invalidTooltip}>{meta.error}</div>}
    </div>
  )
};

export const Checkbox = ({input, label}) => (

    <div className="custom-control custom-checkbox ">
      <input {...input} type="checkbox" className={styles.checkbox}  id="defaultUnchecked"/>
      <label className={""} htmlFor="defaultUnchecked">{label}</label>
    </div>
);

