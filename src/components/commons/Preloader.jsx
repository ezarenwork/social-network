import React from 'react';

const Preloader = () => {
    return (
        <div className="m-auto">
            <div className="spinner-grow" role="status">
                <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow" role="status">
                <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow" role="status">
                <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
};

export default Preloader;