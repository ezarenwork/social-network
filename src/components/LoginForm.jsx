import React from "react";
import {Field, reduxForm} from 'redux-form'
import {Checkbox, Input} from "./commons/FormControls";
import {required, minLength6, email, maxLength30} from "../helpers/validators";
import styles from "../styles/formControls.module.css";


let LoginForm = (props) => {
  return <form onSubmit={props.handleSubmit} >
    <div>
      <Field name="email"
             label="Email"
             component={Input}
             validate={[required,email]}
      />
    </div>
    <div>
      <Field name="password"
             type="password"
             label="Password"
             component={Input}
             validate={[required,minLength6,maxLength30]}
      />
    </div>
    <div>
      <Field  component={Checkbox}
              name="rememberMe"
              label="Remember me"
      />
    </div>
    { props.error &&
       <div className={styles.invalidTooltip}>{props.error}</div>
    }
    { props.captcha &&
        <img src={props.captcha} alt='captcha'/>
    }
    { props.captcha &&
    <Field name="captcha"
           placeholder='Enter symbols'
           component={Input}
           validate={[required]}
    />
    }
    <div className="mt-2">
      <button type="submit" className="btn btn-primary ">Login</button>
    </div>
  </form>
};

LoginForm = reduxForm({form: 'login'})(LoginForm);

export default LoginForm;