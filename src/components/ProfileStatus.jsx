import React, {useState, useEffect} from 'react';

const ProfileStatus = ({updateStatus, status, userId}) => {
    let [editMode, setEditMode] = useState(false);
    let [statusValue, setStatusValue] = useState(status);

    useEffect( () => {
        setStatusValue(status);
        console.log("UseEffect");
    }, [status]);

    const activateEditMode = () => {
        setEditMode(true);
    };

    const deactivateEditMode = () => {
        setEditMode(false);
        updateStatus(statusValue, userId);
    };

    const onStatusChange = (e) => {
        setStatusValue(e.currentTarget.value);
    };
    return <div>
        {!editMode &&
        <div>
            <span onClick={activateEditMode}>{statusValue || "Your status"}</span>
        </div>
        }
        {editMode &&
        <div>
            <input autoFocus={true}
                   value={statusValue}
                   onChange={onStatusChange}
                   onBlur={deactivateEditMode}/>
        </div>
        }
    </div>
};

export default ProfileStatus;