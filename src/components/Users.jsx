import React from 'react';
import User from "./User";
import Pagination from 'react-js-pagination';

let Users = ({
                 currentPage,
                 pageSize,
                 totalUsersCount,
                 onPageChanged,
                 followUser,
                 followingInProgress,
                 unfollowUser,
                 users
             }) => {
    return (
        <div className="col">
            <div className="row">
                <Pagination
                    hideDisabled
                    activePage={currentPage}
                    itemsCountPerPage={pageSize}
                    totalItemsCount={totalUsersCount}
                    pageRangeDisplayed={5}
                    onChange={onPageChanged}
                />
            </div>
            <div className="row">
                <ul className="list-group">
                    {users.map(u =>
                        <User key={u.id}
                              user={u}
                              followingInProgress={followingInProgress}
                              followUser={followUser}
                              unfollowUser={unfollowUser}/>
                    )}
                </ul>
            </div>
        </div>
    );
};


export default Users;



