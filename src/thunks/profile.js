import {profileAPI} from "../api/api";
import {stopSubmit} from "redux-form";
import {setUserPhoto, setUserProfile, setUserStatus} from "../actions";

export const getUserProfile = (userId) => {
    return async (dispatch) => {
        const data = await profileAPI.getProfile(userId);
        dispatch(setUserProfile(data));
    }
};

export const getUserStatus = (userId) => {
    return async (dispatch) => {
        const data = await profileAPI.getProfileStatus(userId);
        dispatch(setUserStatus(data));
    }
};

export const updateStatus = (status, userId) => {
    return async (dispatch) => {
        const data = await profileAPI.updateStatus(status, userId);
        dispatch(setUserStatus(data));
    }
};

export const uploadAvatarImage = (photo) => {
    return async (dispatch) => {
        const data = await profileAPI.updateAvatarImage(photo);
        dispatch(setUserPhoto(data.data.photos));
    }
};


export const updateProfile = (profile) => {
    return async (dispatch, getState) => {
        const userId = getState().auth.userId;
        const data = await profileAPI.updateProfile(profile);
        if (data.resultCode === 0) {
            dispatch(getUserProfile(userId));
        } else {
            dispatch(stopSubmit("editProfileData", {_error: data.messages[0]}));
            return Promise.reject({_error: data.messages[0]});
        }
    }
};