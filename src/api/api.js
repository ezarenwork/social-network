import * as axios from "axios";

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        'API-KEY': 'b0eb75a9-aeea-4d6b-82ee-ade809f06a1d'
    }
});

export const authAPI = {

    getAuth() {
        return instance.get(`auth/me`)
            .then(
                response => response.data
            )
    },

    login(email, password, rememberMe = false, captcha = null) {
        return instance.post(`auth/login`, {email, password, rememberMe, captcha})
            .then(
                response => response.data
            )
    },

    logout() {
        return instance.delete(`auth/login`)
            .then(
                response => response.data
            )
    },

    captcha() {
        return instance.get(`security/get-captcha-url`)
            .then(
            response => response.data
        )
    }

};


export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 6) {
        return instance.get(`users?page=${currentPage}&count=${pageSize}`)
            .then(response => {
                return response.data;
            });
    },
    notFollow(userId) {
        return instance.delete(`follow/${userId}`)
            .then(response => {
                return response.data;
            });
    },
    follow(userId) {
        return instance.post(`follow/${userId}`)
            .then(response => {
                return response.data;
            });
    }
};

export const profileAPI = {
    getProfile(userId) {
        return instance.get(`profile/${userId}`)
            .then(
                response => response.data
            )
    },

    getProfileStatus(userId) {
        return instance.get(`profile/status/${userId}`)
            .then(
                response => response.data
            )
    },
    updateStatus(status, userId) {
        return instance.put(`profile/status`, {status})
            .then(() => instance.get(`profile/status/${userId}`)
                .then(
                    response => response.data
                )
            ).catch(error => console.log(error));
    },
    updateProfile(profile) {
        return instance.put(`profile`, profile)
            .then(
                response => response.data
            )
    },
    updateAvatarImage(photo) {

        const formData = new FormData();
        formData.append("image", photo);
        return instance.put(`profile/photo`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
            .then(
                response => response.data
            )
    }
};
