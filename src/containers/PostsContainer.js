import {addPost} from "../actions";
import ProfilePosts from "../components/ProfilePosts";
import {connect} from "react-redux";

const mapStateToProps = (state) => {
    return {
        posts: state.profile.posts
    }
};
const mapDispatchToProps = {
    addPost
};

const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(ProfilePosts);
export default PostsContainer;