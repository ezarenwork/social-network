import React from 'react';
import Header from "../components/Header";
import {connect} from "react-redux";
import {logout} from "../thunks";

class HeaderContainer extends React.Component {

    render() {
        return <Header {...this.props}/>
    }
}

const mapStateToProps = (state) => {
    return {
        isAuth: state.auth.isAuth,
        login: state.auth.login
    }
};


export default connect(mapStateToProps, {logout})(HeaderContainer);