import {addMessage} from "../actions";
import Messages from "../components/Messages";
import {connect} from "react-redux";
import {withAuthRedirect} from "../hoc/withAuthRedirect";
import {compose} from "redux";

let mapStateToProps = (state) => {
    return {
        dialogs: state.messages.dialogs,
        messages: state.messages.messages
    }
};
let mapDispatchToProps = {
    addMessage
};


export default compose(connect(mapStateToProps, {addMessage}),
    withAuthRedirect)(Messages)