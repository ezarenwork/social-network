import React from "react";
import LoginForm from "../components/LoginForm";
import {compose} from "redux";
import {login} from "../thunks/auth";
import {connect} from 'react-redux';
import {Redirect} from "react-router-dom";


const LoginContainer = ({isAuth, login, captcha}) => {
    const onLoginFormSubmit = (loginData) => {
        login(loginData.email, loginData.password, loginData.rememberMe, loginData.captcha);
    };
    if (isAuth) return <Redirect to='/profile'/>;
    return <div>
        <h1>LOGIN</h1>
        <LoginForm captcha={captcha} onSubmit={onLoginFormSubmit}/>
    </div>
};

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth,
    captcha: state.auth.captcha
});


export default compose(connect(mapStateToProps,
    {
        login
    }))(LoginContainer);