import React from 'react';
import {connect} from 'react-redux';
import Users from '../components/Users';
import Preloader from '../components/commons/Preloader';
import {toggleFollowingProgress} from "../actions";
import {followUser, requestUsers, unfollowUser} from "../thunks";
import {
    getCurrentPage,
    getFollowingInProgress,
    getIsFetching,
    getPageSize,
    getTotalUsersCount,
    getUsers
} from "../selectors/users";

class UsersContainer extends React.Component {
    componentDidMount() {
        this.props.requestUsers(this.props.currentPage, this.props.pageSize);
    }

    onCurrentPage = (pageNumber) => {
        this.props.requestUsers(pageNumber, this.props.pageSize);
    };

    render() {
        return (
            this.props.isFetching
                ? <Preloader/>
                : <Users {...this.props} onPageChanged={this.onCurrentPage}/>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: getUsers(state),
        currentPage: getCurrentPage(state),
        pageSize: getPageSize(state),
        totalUsersCount: getTotalUsersCount(state),
        isFetching: getIsFetching(state),
        followingInProgress: getFollowingInProgress(state)
    };
};


export default connect(mapStateToProps, {
    toggleFollowingProgress,
    requestUsers,
    unfollowUser,
    followUser
})(UsersContainer);

