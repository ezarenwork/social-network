import{
    ADD_MESSAGE,
    UPDATE_NEW_MESSAGE_TEXT
} from "../consts";

export const addMessage= (message) => ({
    type: ADD_MESSAGE,
    payload: message
});

