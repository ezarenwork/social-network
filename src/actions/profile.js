import {ADD_POST, SET_USER_PHOTO, SET_USER_PROFILE, SET_USER_STATUS} from "../consts";

export const addPost = (postText) => ({
    type: ADD_POST,
    payload: postText
});

export const setUserStatus = (status) => ({
    type: SET_USER_STATUS,
    status
});
export const setUserProfile = (profile) => ({
    type: SET_USER_PROFILE,
    profile
});
export const setUserPhoto = (photos) => ({
    type: SET_USER_PHOTO,
    photos
});


