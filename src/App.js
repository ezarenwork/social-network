import React, {useEffect} from 'react';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom";
import {connect} from "react-redux";

import Sidebar from "./components/Sidebar";
import News from "./components/News";
import Music from "./components/Music";
import Settings from "./components/Settings";
import Games from "./components/Games";
import HeaderContainer from "./containers/HeaderContainer";
import Preloader from "./components/commons/Preloader";
import {withSuspense} from "./hoc/withSuspense";
import {initializeApp} from "./reducers/app";

const MessagesContainer = React.lazy(() => import('./containers/MessagesContainer'));
const ProfileContainer = React.lazy(() => import('./containers/ProfileContainer'));
const UsersContainer = React.lazy(() => import('./containers/UsersContainer'));
const LoginContainer = React.lazy(() => import('./containers/LoginContainer'));


const App = ({initializeApp, isInitialized}) => {

    useEffect(() => {
        initializeApp();
    }, [isInitialized]);

    if (!isInitialized) {
        return <Preloader/>
    }

    return (
        <BrowserRouter>
            <div className="box">
                <div className="header">
                    <HeaderContainer/>
                </div>
                <div className="main">
                    <Sidebar/>
                    <div className="content">
                        <Route path='/messages' render={withSuspense(MessagesContainer)}/>
                        <Route path='/profile/:userId' render={withSuspense(ProfileContainer)}/>
                        <Route path='/news' component={News}/>
                        <Route path='/music' component={Music}/>
                        <Route path='/settings' component={Settings}/>
                        <Route path='/games' component={Games}/>
                        <Route path='/users' component={withSuspense(UsersContainer)}/>
                        <Route path='/' component={withSuspense(LoginContainer)}/>
                    </div>
                </div>
                <div className="footer">
                </div>
            </div>
        </BrowserRouter>
    );
};


const mapStateToProps = (state) => ({
    isInitialized: state.app.isInitialized
});


export default connect(mapStateToProps, {initializeApp})(App);
